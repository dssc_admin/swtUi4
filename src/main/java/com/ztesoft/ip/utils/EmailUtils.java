package com.ztesoft.ip.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jodd.io.FileUtil;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.att.ByteArrayAttachment;
import jodd.mail.att.FileAttachment;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.StrUtil;
import com.ztesoft.ip.jobs.Logjobdemo;

public class EmailUtils {

	public String server = null;
	public String email = null;
	public String passwd = null;
	private final static Logger log = Log.get(Logjobdemo.class);

	public void init(String server, String email, String passwd) {
		// TODO Auto-generated method stub
		log.info("初始化邮件发送的帐号信息!");
		this.server = server;
		this.email = email;
		this.passwd = passwd;
	}

	/**
	 * new FileAttachment(new File("pics/png2.png"), "b.jpg", "image/jpeg");
	 * 
	 * @param towho
	 * @param title
	 * @param content
	 * @param attachs
	 * @throws IOException
	 */
	public void sendSimpleEmail(String towho, String title, String content, List<FileAttachment> attachs) throws IOException {
		// TODO Auto-generated method stub
		if (StrUtil.isEmpty(server) || StrUtil.isEmpty(email) || (StrUtil.isEmpty(passwd))) {
			log.warn("不能发送邮件，请初始化发送邮件的帐号和密码!");
			return;
		}
		// 邮件附近
		// EmailAttachment attachment = new FileAttachment(new
		// File("pics/png2.png"), "b.jpg", "image/jpeg");
		Email email = Email.create().from(this.email).to(towho).subject(title).addHtml("<html><META http-equiv=Content-Type content=\"text/html; charset=utf-8\">" + "<body><h1>" + title + "</h1> " + content + "</body></html>");
		if (null != attachs && attachs.size() > 0) {
			for (FileAttachment e : attachs) {
				email.attach(e);
			}
		}
		SmtpServer smtpServer = SmtpServer.create(this.server).authenticateWith(this.email, this.passwd);
		SendMailSession session = smtpServer.createSession();
		session.open();
		session.sendMail(email);
		session.close();
	}

	public void sendImgEmail(String title, String content, List<FileAttachment> attachs) throws IOException {
		// TODO Auto-generated method stub
		if (StrUtil.isEmpty(email) || (StrUtil.isEmpty(passwd))) {
			log.warn("不能发送邮件，请初始化发送邮件的帐号和密码!");
			return;
		}
		// 邮件内容图片标签
		EmailAttachment embeddedAttachment = new ByteArrayAttachment(FileUtil.readBytes("pics/makd.png"), "image/png", "c.png", "c.png");
		// 邮件附近
		EmailAttachment attachment = new FileAttachment(new File("pics/png2.png"), "b.jpg", "image/jpeg");
		Email email = Email.create().from("2642000280@qq.com").to("310336951@qq.com").subject(title).addHtml("<html><META http-equiv=Content-Type content=\"text/html; charset=utf-8\">" + "<body><h1>" + title + "</h1><img src='cid:c.png'>" + content + "</body></html>").embed(embeddedAttachment).attach(attachment).attach(attachment);
		SmtpServer smtpServer = SmtpServer.create("smtp.qq.com").authenticateWith("2642000280@qq.com", "dw222222");
		SendMailSession session = smtpServer.createSession();
		session.open();
		session.sendMail(email);
		session.close();
	}
}
