package com.ztesoft.ip.jobs;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.Log;

public class Logjobdemo implements Runnable {
	private final static Logger log = Log.get(Logjobdemo.class);

	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("job测试hutool日志打印机制 info");
		log.error("job测试hutool日志打印机制 error");
		log.warn("job测试hutool日志打印机制 warn");
		log.debug("job测试hutool日志打印机制 debug");

	}

}
