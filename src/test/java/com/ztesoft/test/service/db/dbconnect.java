package com.ztesoft.test.service.db;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.Setting;
import com.xiaoleilu.hutool.StrUtil;
import com.xiaoleilu.hutool.db.DbUtil;
import com.xiaoleilu.hutool.db.ds.DruidDS;
import com.xiaoleilu.hutool.exceptions.UtilException;

/**
 * http://blog.csdn.net/anxinliu2011/article/details/7560511
 * 
 * @author admin
 *
 */
public class dbconnect {
	private static final Logger log = Log.get(dbconnect.class.getSimpleName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataSource ds3new = null;
		DataSource ds4old = null;
		DruidDS.init(new Setting(new File("resouce/config/druid.setting"), Setting.DEFAULT_CHARSET, false), new Setting(new File("resouce/config/db.setting"), Setting.DEFAULT_CHARSET, false));
		ds3new = DruidDS.getDataSource("oracl3new");
		ds4old = DruidDS.getDataSource("oracl4old");
		Connection conn3 = null;
		Connection conn4 = null;

		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = ds4old.getConnection();
			final DatabaseMetaData metaData = conn.getMetaData();
			System.out.println("conn.getCatalog()" + conn.getCatalog());
			rs = metaData.getTables(conn.getCatalog(), "YCSMART4", null, new String[] { "TABLE" });

			while (rs.next()) {
				final String table = rs.getString("TABLE_NAME");
				final String A = rs.getString("TABLE_TYPE");
				final String B = rs.getString("TABLE_SCHEM");
				final String C = rs.getString("REMARKS");
				if (StrUtil.isBlank(table) == false) {
					// System.out.println(table + "-" + A + "-" + B + "-" + C);
					System.out.println(table);
					String[] dd = DbUtil.getColumnNames(rs);
					System.out.println(dd.length);
				}
			}
		} catch (Exception e) {
			throw new UtilException("Get tables error!", e);
		} finally {
			DbUtil.close(conn, conn3, conn4, rs);
		}
	}

}
